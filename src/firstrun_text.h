/*
	Chess for Casio PRIZM calculators
	Copyright (C) 2013  Balázs Dura-Kovács

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const int firstrun_sorok_szama=23;
const char firstrun_text[] = "Thank you for downloading\nthis chess game to your\ncalculator. I hope you\nwill enjoy it.\n\nPlease read the help be-\nfor playing.\n\nPlease note that this is\nan early release and it\nstill needs testings.\nSo if you find any bugs\nplease report it in email\nto this address:\nbalping.official@gmail.com\nor at Cemetech forum in\nthis thread:\nhttp://goo.gl/wQlzGb\n\n       Thank you!\n     Enjoy the game!\n\n(Press [EXIT].)";
